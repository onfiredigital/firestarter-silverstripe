# OnFireDigital FireStarter Module

This module provides some added configuration and underlying functionality that may be useful to allow clients/agencies to adjust website functionality theirselves. It provides the content management side of the Carousel/Banner, Logos

## Summary of contents

This module provides the following (may not be a definitive list):

* [Carousel/hero image](docs/Carousel.md)
* Upload custom header and footer logos from SiteConfig
* Upload custom favicon and Apple touch logos from SiteConfig

## Installation

```
composer require onfiredigital/firestarter
```

## Requirements

* `silverstripe/framework` 4.0 or above
* `silverstripe/cms` 4.0 or above

## Documentation

### Features

* [Carousel/hero image](docs/Carousel.md)
